
import com.dosideas.webservice.client.GeoIP;
import com.dosideas.webservice.client.GeoIPService;
import com.dosideas.webservice.client.GeoIPServiceSoap;


public class Main {

    public static void main(String[] args) {
        invocarViaJaxb();
    }

    public static void invocarViaJaxb() {
        try {
            GeoIPService service = new GeoIPService();
            GeoIPServiceSoap port = service.getGeoIPServiceSoap();
            
            GeoIP result = port.getGeoIP("166.12.1.1");
            
            System.out.printf("IP: %s // Pais: %s (%s)\n", result.getIP(), result.getCountryName(), result.getCountryCode());
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

}
